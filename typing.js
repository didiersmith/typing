var Trainer = function() {
  this.wordList_ = [];
  this.currentWordIdx_ = 0;
  this.state_ = Trainer.STATES.HALTED;
  this.startTime_ = null;
  this.charsTyped_ = 0;
  this.mistakesMade_ = 0;
}

Trainer.STATES = {
  HALTED: 0,
  NO_MISTAKE: 1,
  MISTAKE: 2
}

Trainer.prototype.reset = function() {
  this.wordList_ = [];
  this.currentWordIdx_ = 0;
  this.state_ = Trainer.STATES.HALTED;
  this.startTime_ = null;
  this.charsTyped_ = 0;
  this.mistakesMade_ = 0;
}

// Load a random quote into the box and reset the current state.
Trainer.prototype.loadQuote = function() {
  // Reset the state, eg. characters typed, etc.
  this.reset();

  // Get the quotes.
  if (quotes.length == 0) {
    console.error("No quotes");
    return;
  }
  var index = Math.floor(Math.random() * quotes.length);
  var quote_arr = quotes[index];
  var quote = quote_arr[0];
  var source = quote_arr[1];
  if (quote.length == 0) {
    console.error("Empty quote at index " + index);
    return;
  }

  // Set up the game.
  wordList = quote.split(" ");
  htmlWordList = [];

  // Do some per-word processing to build up the wordList.
  for (var i = 0; i < wordList.length; i++) {
    var word = wordList[i].trim();
    if (word == '') { continue; }
    // Require the space after each word to advance.
    this.wordList_.push(word + " ");
    htmlWordList.push('<span id="word_' + i + '">' + word + '</span>');
  }
  // Don't require a space to advance the last word.
  var lastWord = this.wordList_[this.wordList_.length - 1];
  this.wordList_[this.wordList_.length - 1] = lastWord.trim();

  quote = htmlWordList.join(" ");

  // Set up the view.
  document.getElementById("words").innerHTML = quote;
  document.getElementById("words_container").scrollTop = 0;
  document.getElementById("source_url").textContent = source;
  document.getElementById("source_url").href = source;
  document.getElementById("new_quote").style.display = "none";
  document.getElementById("entry_box").style.display = "block";
  document.getElementById("entry_box").focus();
}

Trainer.prototype.bindNewQuote = function() {
  var newQuote = document.getElementById("new_quote");
  var this_ = this;
  newQuote.addEventListener("click", function() {
    this_.loadQuote();
  });
}

// Attach the handleInput function to the entry box.
Trainer.prototype.bindEntryBox = function() {
  var entryBox = document.getElementById("entry_box");
  var this_ = this;
  entryBox.addEventListener("input", function() {
    this_.handleInput();
  });
}

// Callback to be called when the text in the input box has changed.
Trainer.prototype.handleInput = function() {
  // Initialize the round when the user begins typing.
  if (this.state_ == Trainer.STATES.HALTED) {
    this.startQuote();
  }
  var entryBox = document.getElementById("entry_box");
  var word = this.wordList_[this.currentWordIdx_];
  var entered = entryBox.value;

  // If they've typed an entire word in correctly.
  if (entered == word) {
    document.getElementById("word_" + this.currentWordIdx_).className = "";
    ++this.currentWordIdx_;
    this.finishWord();
    if (this.currentWordIdx_ == this.wordList_.length) {
      this.finishQuote();
    }
    document.getElementById("word_" + this.currentWordIdx_).className = "active";
    return;
  }

  // If they're part-way through a word, without a mistake.
  if (word.indexOf(entered) == 0) {
    ++this.charsTyped_;

    // Recover from a previous mistake.
    if (this.state_ == Trainer.STATES.MISTAKE) {
      document.body.bgColor = "white";
      this.state_ = Trainer.STATES.NO_MISTAKE;
    }
    return;
  }

  // At this point, the user has made an error.
  if (this.state_ == Trainer.STATES.MISTAKE) {
    return;
  }
  this.state_ = Trainer.STATES.MISTAKE;
  ++this.mistakesMade_;
  this.updateStats();
  document.body.bgColor = "red";
}

// Helper function for when the user has successfully completed a word.
Trainer.prototype.finishWord = function() {
  document.getElementById("entry_box").value = "";
  this.scrollQuote(this.currentWordIdx_, this.wordList_.length);
  this.updateStats();
}

// Update the WPM, CPS, Accuracy stats.
Trainer.prototype.updateStats = function() {
  var currentTime = new Date().getTime();
  var elapsed = (currentTime - this.startTime_) / 1000;

  var wpm = (this.charsTyped_ / 5) / (elapsed / 60);
  var accuracy = 1.0 - (this.mistakesMade_ / this.charsTyped_);
  accuracy = Math.min(accuracy * 100.0, 100.0);

  document.getElementById("stat_wpm").textContent = wpm.toFixed();
  document.getElementById("stat_accuracy").textContent = accuracy.toFixed(1);
}

// Scroll the quote box so that the line the user's typing is approximately
// visible.
Trainer.prototype.scrollQuote = function(current, max) {
  var containerHeight = document.getElementById("words_container").clientHeight;
  var totalHeight = document.getElementById("words").clientHeight;
  var fraction = current / max;

  // How far we've scrolled should be proportional to how far through the
  // quote we are.
  var scrollHeight = fraction * totalHeight;

  // But we want our scroll level to be such that our current position is
  // half-way down the quote viewport.
  scrollHeight = Math.max(scrollHeight - (containerHeight / 2), 0);
  document.getElementById("words_container").scrollTop = scrollHeight;
}

// Helper for recording when the user started typing.
Trainer.prototype.startQuote = function() {
  this.state_ = Trainer.STATES.NO_MISTAKE;
  this.startTime_ = new Date().getTime();
  document.getElementById("word_0").className = "active";
}

// Helper for handling when the user finished the quote.
Trainer.prototype.finishQuote = function() {
  this.state_ = Trainer.STATES.HALTED;
  document.getElementById("entry_box").style.display = "none";
  document.getElementById("new_quote").style.display = "block";
  document.getElementById("new_quote").focus();
}

var TrainerApp = new Trainer();
