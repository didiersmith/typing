#!/usr/bin/python
from unidecode import unidecode

def main():
  ifile = open('quotes.txt', 'r')
  ofile = open('quotes.js', 'w')
  ofile.write("var quotes = [\n")
  url = 'http://about:blank'
  num_lines = 0;
  for line in ifile:
    line = unidecode(line)
    line = line.strip();
    line = line.replace('"', '\\"')
    if len(line) < 10:
      continue
    if line[:4] == 'http':
      url = line
      continue
    num_lines += 1
    out_line = '["%s", "%s"],\n' % (line, url)
    ofile.write(out_line)
  ofile.write(']\n')
  ifile.close()
  ofile.close()
  print "Processed %d lines " % num_lines

if __name__ == '__main__':
  main()
